This program is part of a series of projects I am doing just for fun and practice.
This is the first practice project of the series.

For this project, I am recreating an incomplete text-based Hangman game I made several years ago in Batch.

Purpose:
Learn the capabilities and proper usage of strings, arrays, array lists, imported classes, and exception handling.

Main Goal:
Recreate my old program using Java concepts to get it done in as few lines possible and implement at least two different phrases.

Other Goals:
All possible exceptions are handled.
Allow the use of hyphens.
Make scanning the user input repeat until a valid input is received.
Allow the user to add his or her own words/phrases and hints for custom games - they should only exist while the program is running.
Make the code modular or adaptive - this means that the programmer could add a new possible word/phrase in the game with just one line of code.