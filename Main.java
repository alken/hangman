import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;
import java.util.InputMismatchException;

class Main {

    public static void main(String[] args) {

        int num = 0;
        ArrayList<Hangman> hangmanList = new ArrayList<>();
        Scanner s = new Scanner(System.in);

        // Add more words/phrases and descriptions to the list here. Refer to the comment below.
        // hangmanList.add(new Hangman("Phrase goes here","Description goes here"));
        hangmanList.add(new Hangman("Annapurna", "This is the world's deadliest mountain."));
        hangmanList.add(new Hangman("African bush elephant", "This is the world's largest land animal."));
        hangmanList.add(new Hangman("Willow tree", "This can be used to make aspirin."));
        hangmanList.add(new Hangman("Sphynx", "This is a hairless cat breed."));
        hangmanList.add(new Hangman("Immortal jellyfish", "This is animal can start its life cycle over again when close to death."));

        while (true) {
            clearScreen();

            System.out.println();
            System.out.println("Welcome to Hangman! Select an option.");
            System.out.println();
            System.out.println("1: Random Game");
            System.out.println("2: Custom Game");
            System.out.println("3: Select Game");

            int option;

            do {
                try {
                    option = s.nextInt();
                } catch (InputMismatchException e) {
                    s.nextLine();
                    System.out.println();
                    System.out.println("Input Mismatch! Please enter an integer.");
                    continue;
                }
                s.nextLine();
                if (option > 3 || option < 1) {
                    System.out.println();
                    System.out.println("Invalid Option! Please try again.");
                } else {
                    break;
                }
            } while (true);

            switch (option) {
                case 1:
                    num = new Random().nextInt(hangmanList.size());
                    break;
                case 2:
                    System.out.println();
                    System.out.println("Enter the word/phrase you want answered for your custom game. Only letters, spaces, and hyphens will apply. Everything else will be ignored.");
                    String customPhrase = s.nextLine();
                    System.out.println();
                    System.out.println("Enter the description for your word/phrase.");
                    String customDescription = s.nextLine();
                    hangmanList.add(new Hangman(customPhrase, customDescription));

                    num = hangmanList.size() - 1;
                    break;
                case 3:
                    System.out.println();
                    for (int i = 0; i < hangmanList.size(); i++) {
                        System.out.println((i + 1) + ": " + hangmanList.get(i).getDescription());
                    }

                    int option2;

                    do {
                        try {
                            option2 = s.nextInt() - 1;
                        } catch (InputMismatchException e) {
                            s.nextLine();
                            System.out.println();
                            System.out.println("Input Mismatch! Please enter an integer.");
                            continue;
                        }
                        if (option2 >= hangmanList.size() || option2 < 0) {
                            s.nextLine();
                            System.out.println();
                            System.out.println("Invalid Option! Please try again.");
                        } else {
                            s.nextLine();
                            break;
                        }
                    } while (true);

                    num = option2;
                    break;
            }

            hangmanList.get(num).initPhraseSoFar();

            while (hangmanList.get(num).getWrongGuesses() < 5) {

                clearScreen();

                if (hangmanList.get(num).winCheck()) {
                    System.out.println();
                    System.out.println("Congratulations! You win!");
                    System.out.println("The answer is: " + hangmanList.get(num).getPhrase());
                    break;
                } else {
                    System.out.println();
                    hangmanList.get(num).printStickman();
                    System.out.println();
                    hangmanList.get(num).printLettersSoFar();
                    System.out.println();
                    System.out.println(hangmanList.get(num).getPhraseSoFar());
                    System.out.println();
                    System.out.println(hangmanList.get(num).getDescription());
                    System.out.println();
                    System.out.println("Enter a letter to guess:");

                    char input;

                    do {
                        try {
                            input = s.next("[a-zA-Z]").charAt(0);
                        } catch (InputMismatchException e) {
                            s.nextLine();
                            System.out.println();
                            System.out.println("Input Mismatch! Please enter a character.");
                            continue;
                        }
                        s.nextLine();
                        break;
                    } while (true);

                    hangmanList.get(num).checkGuess(input);
                }
            }
            if (!hangmanList.get(num).winCheck()) {
                System.out.println();
                System.out.println("You lose!");
            }
            System.out.println();
            System.out.println("Play again?");
            System.out.println("1: Yes");
            System.out.println("2: No");

            int again;
            do {
                try {
                    again = s.nextInt();
                } catch (InputMismatchException e) {
                    s.nextLine();
                    System.out.println();
                    System.out.println("Input Mismatch! Please enter an integer.");
                    continue;
                }
                s.nextLine();
                if (again > 2 || again < 1) {
                    System.out.println();
                    System.out.println("Invalid Option! Please try again.");
                } else {
                    break;
                }
            } while (true);

            if (again == 1) {
                hangmanList.get(num).reset();
                continue;
            }
            break;
        }
        s.close();
        System.exit(0);
    }

}

class Hangman {
    private String phrase = "";
    private String description = "";
    private String phraseSoFar;
    private int wrongGuesses = 0;
    private char[] lettersSoFar = new char[26];
    private int letterCount = 0;

    public Hangman(String phrase, String description) {
        setPhrase(phrase);
        setDescription(description);
    }

    public String getPhrase() {
        return phrase;
    }

    public String getDescription() {
        return description;
    }

    public String getPhraseSoFar() {
        return phraseSoFar;
    }

    public int getWrongGuesses() {
        return wrongGuesses;
    }

    public void setPhrase(String phrase) {
        this.phrase = phrase.toUpperCase().replaceAll("\\d", "").replaceAll("[_]", "").replaceAll("[^-\\w\\s]", "");
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void initPhraseSoFar() {
        char[] init = phrase.toCharArray();
        for (int i = 0; i < init.length; i++) {
            if (init[i] != ' ' && init[i] != '-') {
                init[i] = '_';
            }
        }
        phraseSoFar = new String(init);
    }

    public void checkGuess(char guess) {
        boolean isValid = false;
        char[] letters = phrase.toCharArray();
        char[] psf = phraseSoFar.toCharArray();
        for (char c : lettersSoFar) {
            if (c == Character.toUpperCase(guess)) {
                return;
            }
        }
        for (int i = 0; i < letters.length; i++) {
            if (letters[i] == Character.toUpperCase(guess)) {
                psf[i] = letters[i];
                isValid = true;
            }
        }
        phraseSoFar = new String(psf);
        lettersSoFar[letterCount] = Character.toUpperCase(guess);
        letterCount++;
        if (!isValid) {
            wrongGuesses++;
        }
    }

    public void printLettersSoFar() {
        System.out.println("Letters used so far: " + String.valueOf(lettersSoFar));
    }

    public void printStickman() {
        switch (wrongGuesses) {
            case 1:
                System.out.println("---------------");
                System.out.println("l             I");
                System.out.println("l             I");
                System.out.println("l            ( )");
                System.out.println("l");
                System.out.println("l");
                System.out.println("l");
                System.out.println("l");
                System.out.println("l");
                System.out.println("l");
                System.out.println("---");
                break;
            case 2:
                System.out.println("---------------");
                System.out.println("l             I");
                System.out.println("l             I");
                System.out.println("l            ( )");
                System.out.println("l             1");
                System.out.println("l             1");
                System.out.println("l             1");
                System.out.println("l");
                System.out.println("l");
                System.out.println("l");
                System.out.println("---");
                break;
            case 3:
                System.out.println("---------------");
                System.out.println("l             I");
                System.out.println("l             I");
                System.out.println("l            ( )");
                System.out.println("l            /1\\");
                System.out.println("l           / 1 \\");
                System.out.println("l             1");
                System.out.println("l");
                System.out.println("l");
                System.out.println("l");
                System.out.println("---");
                break;
            case 4:
                System.out.println("---------------");
                System.out.println("l             I");
                System.out.println("l             I");
                System.out.println("l            ( )");
                System.out.println("l            /1\\");
                System.out.println("l           / 1 \\");
                System.out.println("l             1");
                System.out.println("l            / \\");
                System.out.println("l           /   \\");
                System.out.println("l          /     \\");
                System.out.println("---");
                break;
            default:
                System.out.println("---------------");
                System.out.println("l             I");
                System.out.println("l             I");
                System.out.println("l");
                System.out.println("l");
                System.out.println("l");
                System.out.println("l");
                System.out.println("l");
                System.out.println("l");
                System.out.println("l");
                System.out.println("---");
                break;
        }
    }

    public boolean winCheck() {
        return phraseSoFar.equals(phrase);
    }

    public void reset() {
        initPhraseSoFar();
        wrongGuesses = 0;
        lettersSoFar = new char[26];
        letterCount = 0;
    }
}
